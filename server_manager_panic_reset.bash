#!/bin/bash
# handler for factorio-server-manager panic
# TODO: remove excess headers, check if logged in. ENV or arguments for input

container_name=''
rcon_password=''
api_user=''
api_password=''

function api_login() {
curl 'https://factorio.acd.ninja/api/login' \
  --cookie-jar MANAGER_COOKIE \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Origin: https://factorio.acd.ninja' \
  -H 'Accept-Encoding: gzip, deflate, br' \
  -H 'Accept-Language: en-US,en;q=0.9' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' \
  -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Referer: https://factorio.acd.ninja/login' \
  -H 'X-Requested-With: XMLHttpRequest' \
  -H 'Connection: keep-alive' \
  --data "{\"username\":\"$api_user\",\"password\":\"$api_password\"}" \
  --compressed 2>/dev/null
}

function api_get_settings() {
curl 'https://factorio.acd.ninja/api/settings' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Accept-Encoding: gzip, deflate, br' \
  -H 'Accept-Language: en-US,en;q=0.9' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Referer: https://factorio.acd.ninja/config' \
  -H 'X-Requested-With: XMLHttpRequest' \
   --cookie MANAGER_COOKIE 2>/dev/null
}

function api_status() {
status="$(curl 'https://factorio.acd.ninja/api/server/status' --cookie MANAGER_COOKIE \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Accept-Encoding: gzip, deflate, br' \
  -H 'Accept-Language: en-US,en;q=0.9' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Referer: https://factorio.acd.ninja/login' \
  -H 'X-Requested-With: XMLHttpRequest' \
  -H 'Connection: keep-alive' \
  --compressed 2>/dev/null)"

jq . <<< "$status"
}

function api_running() {
success="$(api_status | jq .success)"
if [ "$success" = 'true' ]
then
  running="$(api_status | jq .data.status)"
  if [ "$running" = '"running"' ]
  then
    return 0
  else
    return 1
  fi
fi
}

function api_start_server() {
response="$(curl 'https://factorio.acd.ninja/api/server/start' \
  --cookie MANAGER_COOKIE \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0' \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Accept-Language: en-US,en;q=0.5' \
  --compressed -H 'Referer: https://factorio.acd.ninja/' \
  -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
  -H 'X-Requested-With: XMLHttpRequest' \
  -H 'DNT: 1' \
  -H 'Connection: keep-alive' \
  --data '{"bindip":"0.0.0.0","savefile":"Load Latest","port":34197}' 2>/dev/null)"

success="$(jq .success <<< "$response")"

if [ "$success" = 'true' ]
then
  api_running
fi
}

function api_stop_server() {
curl 'https://factorio.acd.ninja/api/server/stop' \
  --cookie MANAGER_COOKIE \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Accept-Encoding: gzip, deflate, br' \
  -H 'Accept-Language: en-US,en;q=0.9' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36' \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Referer: https://factorio.acd.ninja/' \
  -H 'X-Requested-With: XMLHttpRequest' \
  -H 'Connection: keep-alive' \
  --compressed 2> /dev/null
}

function rcon_get_password() {
docker logs "$container_name" 2>&1 | head -30 | grep -o 'rcon-password [^-]*' | cut -d ' ' -f2
}

function rcon_save_game() {
python3 -c "import factorio_rcon
client = factorio_rcon.RCONClient('127.0.0.1', 43081, '$rcon_password')
response = client.send_command('/save panic_save_"$(date +%m_%d_%H_%M)"')"
}

function rcon_terminate_server() {
python3 -c "import factorio_rcon
client = factorio_rcon.RCONClient('127.0.0.1', 43081, '$rcon_password')
response = client.send_command('/quit')"
}

function container_recreate() {
  rcon_save_game
  rcon_terminate_server
  api_stop_server
# stop and remove container
  docker stop "$container_name" && docker rm "$container_name"
# recreate container
  docker run --name "$container_name" -d \
    -v /root/factorio/0/saves:/opt/factorio/saves \
    -v /root/factorio/0/mods:/opt/factorio/mods \
    -v /root/factorio/0/config:/opt/factorio/config \
    -v /root/factorio/0/security:/security \
    -v /root/factorio/0/auth:/opt/factorio-server-manger/auth.leveldb \
    -p 8080:80 \
    -p 4433:443 \
    -p 34197:34197/udp \
    -p 43081:43081/tcp \
    factorio_server:latest
#"$container_name"_factorio_server:"$version_tag" TODO

  api_start_server
}

function container_check_error() {
container_log="$(docker logs "$container_name" 2>&1)"
container_grep="$(grep -B2 -A6 'panic:' <<< "$container_log")"

if [ -n "$container_grep" ]
then
  echo "$container_log" >> log_"$(date +%dmHM)"
  echo "$container_grep" >> panic_"$(date +%dmHM)"
  container_recreate
  return 1
else
  return 0
fi
}

#api_login
#api_status
#api_get_settings
