#!/bin/bash
# nginx restart script

check_domain='acd.ninja'
connect_timeout=2
max_time=2
reset_wait_minutes=5

# check locally first
if ! curl --connect-timeout "$connect_timeout" --max-time "$max_time" 127.0.0.1 &> /dev/null
then
  if ! curl --connect-timeout "$connect_timeout" --max-time "$max_time" "$check_domain" &> /dev/null
  then
    if ! find . -cmin -"$reset_wait_minutes" -name restart_nginx | read
    then
      echo 'restarting nginx'
      touch restart_nginx
      service nginx restart
    else
      echo "still no connection, waiting $reset_wait_minutes minutes between each  attempt"
    fi
  fi
fi

