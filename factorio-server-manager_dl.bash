#!/bin/bash
# download all versions of factorio-server-manager
# https://github.com/mroote/factorio-server-manager/releases

server_manager="$(curl https://github.com/mroote/factorio-server-manager/releases 2> /dev/null \
 | grep -io "/mroote/factorio-server-manager/releases/download/[0-9].[0-9].[0-9]/[a-z0-9.-]*" \
  | grep -v 'windows')"

while read -r line
do
  version="$(echo "$line"  | cut -d / -f6)"
  wget https://github.com"$line" -O factorio-server-manager-linux_"$version".zip
done <<< "$server_manager"

