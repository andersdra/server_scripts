#!/bin/bash
# keep local copies of factorio headless installs
# lots of sleeping to get through..

headless_versions="$(curl https://www.factorio.com/download-headless 2> /dev/null \
 | grep -io "/get-download/[0-9].[0-9]*.[0-9]*/headless/linux64")"

while read -r line
do
  version="$(echo "$line"  | cut -d / -f3)"
  version_folder="/get-download/$version/headless"
  if [ ! -d ."$version_folder" ];then
    echo 'making directory'
    mkdir --parent ."$version_folder"
    echo "downloading $version"
    curl --location https://factorio.com"$line" -o ".$version_folder"/linux64
  else
    if [ ! -f ."$version_folder"/linux64 ];then
      echo "downloading server $version"
      curl -L ."$version_folder"/linux64 https://factorio.com"$line" 2> /dev/null -o ."$version_folder"/linux64
    else
      # file already exists, get size of server through download header
      server_url="$(curl -L https://factorio.com/"$version_folder"/linux64 -I 2> /dev/null | \
       gawk -v IGNORECASE=1 '/^Location/ { print $2 }' | tr -d '\r')"
      sleep 10
      dl_size="$(curl "$server_url" --head 2> /dev/null | gawk -v IGNORECASE=1 '/^Content-Length/ { print $2 }' | tr -d '\r')"
      while [ "$dl_size" -eq '206' ];do
          echo 'SIZE==206'
          sleep 10
          dl_size="$(curl "$server_url" -I 2> /dev/null | gawk -v IGNORECASE=1 '/^Content-Length/ { print $2 }' | tr -d '\r')"
      done
      # local file
      file_size=$(wc -c < ."$version_folder"/linux64)
      if [ "$dl_size" = "$file_size" ]
      then
        if [ -f sha1sums ];then
          saved_shasum=$(grep "$version" sha1sums)
          if echo "$saved_shasum" | sha1sum --status --check -
          then
            echo "shasum match $version"
            echo "$saved_shasum" >> sha1sums.append
          fi
        else
          echo "calculating sha1sum"
          sha1sum -b "$PWD$version_folder"/linux64 >> sha1sums.append
        fi
      else
        echo "wrong size"
        sleep 2
        echo "downloading server $version"
        curl  --location "$server_url" -o ."$version_folder"/linux64
        echo "calculating sha1sum"
        sha1sum -b ."$version_folder"/linux64 >> sha1sums.append
      fi
    fi
  fi
sleep 5
done <<< "$headless_versions"

sort --unique sha1sums.append > sha1sums
rm sha1sums.append

